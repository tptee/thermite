use itertools::{EitherOrBoth::*, Itertools};
use std::cell::RefCell;
use std::rc::Rc;
use vec_tree::{Index, VecTree};
use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;
use web_sys::IdleDeadline;

// A macro to provide `println!(..)`-style syntax for `console.log` logging.
macro_rules! log {
    ( $( $t:tt )* ) => {
        web_sys::console::log_1(&format!( $( $t )* ).into());
    }
}

fn window() -> web_sys::Window {
    web_sys::window().expect("no global `window` exists")
}

fn document() -> web_sys::Document {
    window()
        .document()
        .expect("should have a document on window")
}

#[derive(Debug)]
enum Element {
    HtmlElement(HtmlElement),
    Text(TextElement),
}

#[derive(Debug)]
struct Props {
    example: String,
    text_content: Option<String>,
    children: Option<Vec<Element>>,
}

#[derive(Debug)]
struct HtmlElement {
    dom_type: String,
    props: Option<Props>,
}

#[derive(Debug)]
struct TextElement {
    props: Option<Props>,
}

#[derive(Debug)]
enum Effect {
    Update,
    Placement,
    Deletion,
}

#[derive(Debug)]
struct Fiber {
    kind: String,
    dom: Option<web_sys::Node>,
    props: Option<Props>,
    alternate: Option<Index>,
    effect_tag: Option<Effect>,
}

fn create_element(dom_type: String, mut props: Props, children: Vec<Element>) -> Element {
    props.children = Some(children);
    Element::HtmlElement(HtmlElement {
        dom_type,
        props: Some(props),
    })
}

fn render(
    element: Element,
    container: web_sys::Node,
    current_root: Option<Index>,
    wip_tree: &mut VecTree<Fiber>,
    deletions: &mut Vec<Index>,
) -> Result<Index, JsValue> {
    deletions.clear();

    let wip_root_index = wip_tree.insert_root(Fiber {
        kind: "div".into(),
        dom: Some(container),
        props: Some(Props {
            text_content: None,
            example: "root".into(),
            children: Some(vec![element]),
        }),
        alternate: current_root,
        effect_tag: None,
    });

    Ok(wip_root_index)
}

fn get_updated_fiber(old_fiber: &Fiber, old_fiber_idx: Index, element: &mut Element) -> Fiber {
    Fiber {
        kind: old_fiber.kind.clone(),
        props: match element {
            Element::HtmlElement(element) => element.props.take(),
            Element::Text(text) => text.props.take(),
        },
        // TODO: remove clone (research if this is a real copy since it's a GC handle)
        dom: old_fiber.dom.clone(),
        alternate: Some(old_fiber_idx),
        effect_tag: Some(Effect::Update),
    }
}

fn get_new_fiber(element: &mut Element) -> Fiber {
    Fiber {
        kind: match element {
            Element::HtmlElement(element) => element.dom_type.clone(),
            Element::Text(_) => "TEXT".into(),
        },
        props: match element {
            Element::HtmlElement(element) => element.props.take(),
            Element::Text(text) => text.props.take(),
        },
        dom: None,
        alternate: None,
        effect_tag: Some(Effect::Placement),
    }
}

fn reconcile_children(
    wip_fiber_idx: Index,
    mut children: Vec<Element>,
    wip_tree: &mut VecTree<Fiber>,
    current_tree: &mut VecTree<Fiber>,
    deletions: &mut Vec<Index>,
) {
    log!("Reconciling children");
    let wip_fiber = wip_tree
        .get(wip_fiber_idx)
        .expect("WIP fiber removed from arena unexpectedly");

    // Use trait objects for iterators to allow empty iterator in `zip_longest`
    // TODO: refactor to avoid allocations
    let old_fiber_children: Box<dyn Iterator<Item = (&Fiber, Index)>> = wip_fiber
        .alternate
        .map(|idx| {
            let old_fibers = current_tree
                .children(idx)
                .filter_map(|idx| current_tree.get(idx).map(|fiber| (fiber, idx)));
            Box::new(old_fibers) as Box<dyn Iterator<Item = (&Fiber, Index)>>
        })
        .unwrap_or_else(|| Box::new(std::iter::empty::<(&Fiber, Index)>()));

    for pair in children.iter_mut().zip_longest(old_fiber_children) {
        let new_fiber: Option<Fiber> = match pair {
            Both(element, (old_fiber, old_fiber_idx)) => {
                let is_same_type = match element {
                    Element::HtmlElement(element) => element.dom_type == old_fiber.kind,
                    Element::Text(_) => old_fiber.kind == "TEXT",
                };

                if is_same_type {
                    Some(get_updated_fiber(old_fiber, old_fiber_idx, element))
                } else {
                    deletions.push(old_fiber_idx);
                    Some(get_new_fiber(element))
                }
            }
            Left(element) => Some(get_new_fiber(element)),
            Right((_, old_fiber_idx)) => {
                deletions.push(old_fiber_idx);
                None
            }
        };

        if let Some(new_fiber) = new_fiber {
            wip_tree.insert(new_fiber, wip_fiber_idx);
        }
    }
}

fn perform_unit_of_work(
    own_fiber_idx: Index,
    mut wip_tree: &mut VecTree<Fiber>,
    mut current_tree: &mut VecTree<Fiber>,
    deletions: &mut Vec<Index>,
) -> Option<Index> {
    // Borrow as mutable...
    {
        let own_fiber = wip_tree.get_mut(own_fiber_idx)?;

        // Get or create the fiber's own DOM
        own_fiber.dom = own_fiber
            .dom
            .take()
            .or_else(|| match own_fiber.kind.as_ref() {
                "TEXT" => {
                    if let Some(text_content) = own_fiber
                        .props
                        .as_ref()
                        .and_then(|props| props.text_content.as_ref())
                    {
                        Some(
                            document()
                                .create_text_node(text_content)
                                .dyn_into::<web_sys::Node>()
                                .ok()?,
                        )
                    } else {
                        None
                    }
                }
                _ => Some(
                    document()
                        .create_element(&own_fiber.kind)
                        .ok()?
                        .dyn_into::<web_sys::Node>()
                        .ok()?,
                ),
            });
    }
    // ...then reborrow as immutable.
    let own_fiber = wip_tree.get(own_fiber_idx)?;

    // If this fiber has a parent DOM node, append its DOM as a child
    let parent_dom = wip_tree
        .parent(own_fiber_idx)
        .and_then(|parent_idx| wip_tree.get(parent_idx))
        .and_then(|parent| parent.dom.as_ref());

    match (own_fiber.dom.as_ref(), parent_dom) {
        (Some(own_dom), Some(parent_dom)) => {
            log!("Adding fiber DOM node as child of parent DOM node");
            parent_dom
                .append_child(&own_dom)
                .expect("Could not add fiber node as a child of parent DOM node");
        }
        _ => {}
    }

    // Reborrow AGAIN as mutable.
    let own_fiber = wip_tree.get_mut(own_fiber_idx)?;

    // Create a fiber for each child, setting the first child as the parent fiber's child
    // and linking subsequent children to the first child as siblings
    if let Some(children) = own_fiber
        .props
        .as_mut()
        .and_then(|props| props.children.take())
    {
        reconcile_children(
            own_fiber_idx,
            children,
            &mut wip_tree,
            &mut current_tree,
            deletions,
        );
    }

    // Find the next unit of work
    if let Some(first_child) = wip_tree.children(own_fiber_idx).nth(0) {
        return Some(first_child);
    }

    let mut next_fiber_idx = Some(own_fiber_idx);
    while let Some(next_idx) = next_fiber_idx {
        if let Some(sibling) = wip_tree.following_siblings(next_idx).nth(0) {
            return Some(sibling);
        }
        next_fiber_idx = wip_tree.parent(next_idx);
    }

    None
}

fn commit_root(
    wip_tree: &mut VecTree<Fiber>,
    current_tree: &mut VecTree<Fiber>,
    deletions: &mut Vec<Index>,
) {
    for idx in deletions.iter() {
        if let Some(fiber) = wip_tree.get_mut(*idx) {
            fiber.effect_tag = Some(Effect::Deletion);
        }
        commit_work(*idx, wip_tree);
    }

    if let Some(root_idx) = wip_tree.get_root_index() {
        let mut traversal = wip_tree.descendants(root_idx);

        // Skip the root node
        traversal.next();

        for fiber_idx in traversal {
            commit_work(fiber_idx, wip_tree);
        }
    }

    std::mem::swap(current_tree, wip_tree);
    wip_tree.clear();
}

fn commit_work(fiber_idx: Index, wip_tree: &VecTree<Fiber>) {
    let own_fiber = wip_tree.get(fiber_idx);

    let parent_dom = wip_tree
        .parent(fiber_idx)
        .and_then(|idx| wip_tree.get(idx))
        .and_then(|fiber| fiber.dom.as_ref());

    let own_dom = own_fiber.and_then(|fiber| fiber.dom.as_ref());

    match (parent_dom, own_dom, own_fiber) {
        (Some(parent_dom), Some(own_dom), Some(own_fiber)) => match own_fiber.effect_tag {
            Some(Effect::Placement) => {
                log!("Appending DOM node");
                parent_dom
                    .append_child(&own_dom)
                    .expect("Could not append own DOM as child to parent DOM");
            }
            Some(Effect::Update) => {
                log!("Would have updated node");
            }
            Some(Effect::Deletion) => {
                log!("Deleting DOM node");
                parent_dom
                    .remove_child(&own_dom)
                    .expect("Could not remove own DOM as child to parent DOM");
            }
            None => {}
        },
        _ => {}
    }
}

// This function is automatically invoked after the wasm module is instantiated.
#[wasm_bindgen(start)]
pub fn run() -> Result<(), JsValue> {
    console_error_panic_hook::set_once();
    let root = create_element(
        "div".into(),
        Props {
            text_content: None,
            example: "root".into(),
            children: Some(Vec::new()),
        },
        vec![Element::Text(TextElement {
            props: Some(Props {
                text_content: Some("Hello from Build Your Own React in Rust!".into()),
                example: "yeet".into(),
                children: Some(vec![]),
            }),
        })],
    );

    let node = document().body().unwrap().dyn_into::<web_sys::Node>()?;

    let mut deletions = vec![];

    let mut current_tree = VecTree::new();
    let mut wip_tree = VecTree::new();

    let current_root = None;
    let wip_root = render(root, node, current_root, &mut wip_tree, &mut deletions)?;

    let mut next_unit_of_work = Some(wip_root);

    // let fib = fiber_arena.insert(first_fiber);
    // let loop_closure = Closure::wrap(Box::new(move |deadline: IdleDeadline| {
    //     work_loop(deadline).map_err(|_| {});
    // }) as Box<dyn FnMut(_)>);
    // window().request_idle_callback(loop_closure.as_ref().unchecked_ref());
    // Here we want to call `requestAnimationFrame` in a loop, but only a fixed
    // number of times. After it's done we want all our resources cleaned up. To
    // achieve this we're using an `Rc`. The `Rc` will eventually store the
    // closure we want to execute on each frame, but to start out it contains
    // `None`.
    //
    // After the `Rc` is made we'll actually create the closure, and the closure
    // will reference one of the `Rc` instances. The other `Rc` reference is
    // used to store the closure, request the first frame, and then is dropped
    // by this function.
    //
    // Inside the closure we've got a persistent `Rc` reference, which we use
    // for all future iterations of the loop
    let f: Rc<RefCell<Option<Closure<dyn FnMut(_)>>>> = Rc::new(RefCell::new(None));
    let g = f.clone();

    *g.borrow_mut() = Some(Closure::wrap(Box::new(move |deadline: IdleDeadline| {
        let mut should_yield = false;
        while next_unit_of_work.is_some() && !should_yield {
            let next = next_unit_of_work.take().unwrap();
            next_unit_of_work =
                perform_unit_of_work(next, &mut wip_tree, &mut current_tree, &mut deletions);
            should_yield = deadline.time_remaining() < 1.0;
            if should_yield {
                log!("Yielding due to deadline");
            }
        }

        if next_unit_of_work.is_none() && wip_tree.get_root_index().is_some() {
            log!("Finished work, committing");
            commit_root(&mut wip_tree, &mut current_tree, &mut deletions);
        }

        window().request_idle_callback(f.borrow().as_ref().unwrap().as_ref().unchecked_ref());
    }) as Box<dyn FnMut(_)>));

    window().request_idle_callback(g.borrow().as_ref().unwrap().as_ref().unchecked_ref());
    Ok(())
}
